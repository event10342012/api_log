import logging
from datetime import datetime

from .utils import make_log_record, notify
from flask import Flask, request, current_app
from flask_appbuilder import AppBuilder, SQLA

logging.basicConfig(format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")
logging.getLogger().setLevel(logging.DEBUG)

app = Flask(__name__)
app.config.from_object("config")
db = SQLA(app)
appbuilder = AppBuilder(app, db.session)


@app.post('/log')
def log():
    from .models import LogRecord
    formatter = logging.Formatter('[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s')

    data = request.form
    record = make_log_record(data)
    msg = formatter.format(record)

    # notify bu
    if record.levelno == 30:
        notify(msg)

    # notify system admin
    if record.levelno >= 40:
        notify(msg)

    session = current_app.appbuilder.session
    record = LogRecord(create_time=datetime.fromtimestamp(record.created), level=record.levelname, message=msg)

    session.add(record)
    session.commit()
    return {'status': 'success'}


from . import views

appbuilder.add_view(views.LogRecordModelView, 'Logs')
