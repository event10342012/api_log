import ast
import logging
import requests
import config


def notify(message):
    headers = {"Authorization": "Bearer " + config.LINE_NOTIFY_TOKEN}
    data = {'message': message}

    # 以 requests 發送 POST 請求
    requests.post("https://notify-api.line.me/api/notify",
                  headers=headers, data=data)


def make_log_record(data: dict) -> logging.LogRecord:
    record = logging.makeLogRecord(data)
    record.created = float(record.created)
    record.levelno = int(record.levelno)
    record.lineno = int(record.lineno)
    record.msecs = float(record.msecs)
    record.relativeCreated = float(record.relativeCreated)
    record.thread = int(record.thread)
    record.process = int(record.process)
    record.args = ast.literal_eval(record.args)
    for var in record.__dict__:
        if getattr(record, var) == 'None':
            setattr(record, var, None)
    return record
