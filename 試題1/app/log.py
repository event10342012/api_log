import logging
import logging.handlers
import queue

que = queue.Queue(-1)
formatter = logging.Formatter('[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s')


def start_log_listener():
    hh = logging.handlers.HTTPHandler('log_collector:8080', '/log', method='POST')
    hh.setLevel(logging.INFO)

    listener = logging.handlers.QueueListener(que, hh)
    listener.start()


start_log_listener()


def create_logger(name=None):
    # create logger
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    # create console and socket handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)

    qh = logging.handlers.QueueHandler(que)

    # add ch, sh to logger
    logger.addHandler(ch)
    logger.addHandler(qh)
    return logger
