from flask_appbuilder import Model
from sqlalchemy import Column, Integer, String, DateTime


class LogRecord(Model):
    id = Column(Integer, primary_key=True)
    create_time = Column(DateTime)
    level = Column(String(20))
    message = Column(String(5000))
