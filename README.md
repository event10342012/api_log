## Quick start

1. 申請[line notify](https://notify-bot.line.me/zh_TW/)申請token 並新增到.env檔案

    登入line帳號 -> 右上角 個人頁面 -> 發行權杖 -> 選擇傳送給自己
    ```dotenv
    LINE_NOTIFY_TOKEN=${TOKEN}
    ```
   
2. 產生SECRET_KEY並新增到.env檔案

   ```python
   import secrets
   print(secrets.token_urlsafe(32))
   # 812016ed3f67661c0ac934ebb1f86679
   ```
   paste to .env
   
   ```dotenv
   SECRET_KEY=812016ed3f67661c0ac934ebb1f86679
   ```

3. 啟動服務

    ```shell
    $ docker-compose up -d 
    ```

4. 訪問服務

    - log_collector web [http://localhost](http://localhost)
      - 帳號密碼：admin/admin
    - API add [http://localhost/api/add](http://localhost/api/add)

    呼叫 /api/add
    ```shell
    $ curl -X 'POST' 'http://localhost/api/add' \
      -H 'accept: application/json' \
      -H 'Content-Type: application/json' \
      -d '{"a": 10, "b": 2}'
    ```


## 試題3

![System architecture](./試題3/system%20architecture.png)
