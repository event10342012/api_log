from flask import render_template
from flask_appbuilder import ModelView
from flask_appbuilder.models.sqla.interface import SQLAInterface

from . import appbuilder, db
from . import models


class LogRecordModelView(ModelView):
    datamodel = SQLAInterface(models.LogRecord)

    list_columns = ['create_time', 'level', 'message']

    include_route_methods = {'list'}

    base_order = ('create_time', 'desc')


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return (
        render_template(
            "404.html", base_template=appbuilder.base_template, appbuilder=appbuilder
        ),
        404,
    )


db.create_all()
