from fastapi import FastAPI
from pydantic import BaseModel

from log import create_logger

app = FastAPI()

logger = create_logger()


class Item(BaseModel):
    a: int
    b: int


@app.post('/api/add')
def add(item: Item):
    """sum two arguments
    """
    result = item.a + item.b
    logger.info(f'{item.a} + {item.b} = {result}')

    if result > 100:
        logger.warning(f'a + b = {result} is larger than 100')
    return {'sum': result}


@app.get('/api/test')
def test():
    logger.info('test')
    return "test"


@app.get('/api/error')
def error():
    raise ValueError('error test')
